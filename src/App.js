import React,{useState} from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import './utils/courier-std.css';
import DrawForm from "./components/DrawForm";
function App() {

  const [state,setState] = useState({
    combination : '',
    norows : 3,
    direction : '',
    errorMsg : '',
    combination_error : '',
    norows_error : '',
    direction_error : ''
  })

  return (
    
      <div className="App">
        <DrawForm state = {state} setState = {setState} />        
      </div>
      
  );
}

export default App;
