import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

const ModalExample = ({modal,toggle,errorMsg}) => { 

  const closeBtn = <button className="close" onClick={toggle}>&times;</button>;

  return (
  <div>    
    <Modal isOpen={modal} toggle={toggle}>
      <ModalHeader toggle={toggle} close={closeBtn} className="text-danger">Invalid Input!</ModalHeader>
      <ModalBody>
        {errorMsg}
      </ModalBody>
      <ModalFooter>        
        <Button color="secondary" onClick={toggle}>Close</Button>
      </ModalFooter>
    </Modal>
  </div>
);
}

export default ModalExample;