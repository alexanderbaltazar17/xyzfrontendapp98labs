import React,{useState} from 'react';
import {Container, Row, Col, Button, Form, FormGroup, Label, Input} from "reactstrap";
import XYZModal from "./XYZModal"
import DrawXYZ from "./DrawXYZ"

function DrawForm({state,setState}) {

    const [modal, setModal] = useState(false);
    const [draw, setDraw] = useState(false);
    const [tempState, setTempState] = useState(state);  

    const toggle = () => setModal(!modal);

    const handleChange = (event) => {
        var {name, value} = event.target;
        setState({...state,[name] : value});   
    }

    const handleSubmit = (event) => {        
        event.preventDefault();                
        if(/[^xyz]/gi.test(state.combination) || state.combination === ""){
            setState({
                ...state,
                errorMsg: "Letter Combination must only consist of Letters X,Y, and Z!",
                combination_error : "is-invalid"
            })            
			toggle();
        }
        else if( (state.norows%2 === 0) || (state.norows < 3) || !(/^\d+$/.test(state.norows)) ){            
            setState({
                ...state,
                errorMsg: "Number of Rows Must be Greater than 3 and an Odd Number!",
                norows_error : "is-invalid"
            })            
            toggle();
        }
        else if(state.direction === ""){
            setState({
                ...state,
                errorMsg: "Please Choose Direction!",                
                direction_error : "is-invalid",
             })            
            toggle();			
        }
        else{
            setState({
                ...state,
                errorMsg: "",
                combination_error : "",
                norows_error : "",
                direction_error : ""
            })
            setDraw(true);
            setTempState({ ...state });            
        }        

    }

    return (
        <>
            <XYZModal toggle = {toggle} modal={modal} errorMsg = {state.errorMsg} ></XYZModal>
            <Container className="mt-5 mb-5">

                <Col style={{border:"1px solid #ccc", padding:"20px", marginBottom:"20px"}} md={6}>
                    <Form onSubmit={handleSubmit}>
                        <Row >
                            <Col>
                                <FormGroup className="form-row">
                                    <Col sm={3}>
                                        <Label for="combination" className="col-form-label">Combination</Label>
                                    </Col>
                                    <Col md={9}>
                                        <Input 
                                            type="text" 
                                            name="combination"
                                            id="combination"
                                            placeholder="Enter Combination" 
                                            className = {state.combination_error}
                                            value = {state.combination}
                                            onChange = {handleChange}
                                        />
                                    </Col>
                                </FormGroup>
                            </Col>
                        </Row>

                        <Row>
                            <Col>
                                <FormGroup className="form-row">
                                    <Col sm={3}>
                                        <Label for="norows" className="col-form-label">No. of Rows</Label>
                                    </Col>
                                    <Col md={9}>
                                        <Input 
                                            type="number" 
                                            min="3" 
                                            id="norows" 
                                            name="norows" 
                                            placeholder="Enter Number of Rows"
                                            className = {state.norows_error}
                                            value = {state.norows}
                                            onChange = {handleChange}
                                        />
                                    </Col>
                                </FormGroup>
                            </Col>
                        </Row>

                        <Row>
                            <Col>
                            <FormGroup className="form-row">
                                <Col md={3}>
                                    <Label for="direction" className="col-form-label">Direction</Label>
                                </Col>
                                <Col md={9}>
                                    <Input 
                                        type="select" 
                                        name="direction" 
                                        id="direction"
                                        value={state.direction || ''}
                                        className = {state.direction_error}
                                        onChange = {handleChange}                                         
                                    >
                                        <option value="">Select Direction</option>
                                        <option value="Horizontal">Horizontal</option>
                                        <option value="Vertical">Vertical</option>                            
                                    </Input>
                                </Col>
                            </FormGroup>
                            </Col>
                        </Row>

                        <Row>
                            <Col>
                                <FormGroup className="form-row">
                                    <Col md={3} className="ml-auto">
                                        <Button color="primary" className="btn btn-block">Submit</Button>
                                    </Col>                           
                                </FormGroup>
                            </Col>
                        </Row>
                    </Form>                
                </Col>
                
                <DrawXYZ state = {tempState} draw={draw} />

            </Container>
        </>
    )
}
export default DrawForm