import React from 'react'
import {Container} from "reactstrap"
import {v4} from "uuid";
function DrawXYZ({state,draw}) {

    const splitCombination = [];
    for(var x = 0; x < state.combination.length; x++){
        splitCombination[x] = state.combination.charAt(x);
    }

    var direction = state.direction === "Horizontal" ? "flex-row " : "flex-column ";
    direction += "d-flex justify-content-around flex-wrap";

    const drawXYZ = splitCombination.map(x => {
        var XYZ = [];
        var	half = Math.floor(state.norows/2);

        switch(x){            
            case 'X':
            case 'x':            
            for(var row = 0; row < state.norows ; row++){
                for(var column = 0; column < state.norows; column++){
                    if( (row === column) || ((row+column) === (state.norows-1)) ){
                        XYZ.push(<span key={v4()}>o</span>);
                    }
                    else{
                        XYZ.push(<span key={v4()}>&nbsp;</span>);
                    }			
                }
                XYZ.push(<div key={v4()} style={{marginBottom:"10px;"}}/>);      
            }            
            break;
            case 'Y':
            case 'y':                        
        
            for(row = 0; row < state.norows ; row++){
                for(column = 0; column < state.norows; column++){
                    if( (row <= half) && (row === column || ( (row+column) === (state.norows-1) ) ) ) {                       
                        XYZ.push(<span key={v4()}>o</span>);
                    }
                    else if( (row > half) && (column === half)){                        
                        XYZ.push(<span key={v4()}>o</span>);
                    }
                    else{                        
                        XYZ.push(<span key={v4()}>&nbsp;</span>);
                    }
                }                
                XYZ.push(<div key={v4()} style={{marginBottom:"10px;"}}/>);          
            
            }
            break;
            case 'Z':
            case 'z':
            for(row = 0; row < state.norows ; row++){
                for(column = 0; column < state.norows; column++){
                    if( (row === 0 || row === (state.norows-1) ) ){                        
                        XYZ.push(<span key={v4()}>o</span>);
                    }
                    else if( !(row === 0 || row === (state.norows-1)) && (row+column === state.norows-1) ){                        
                        XYZ.push(<span key={v4()}>o</span>);
                    }else{                        
                        XYZ.push(<span key={v4()}>&nbsp;</span>);
                    }
                }                
                XYZ.push(<div key={v4()} style={{marginBottom:"10px;"}}/>);      
            }
            break;
            default: return XYZ
        }

        return (
            <div key={v4()} className="justify-content-center align-items-center">
                {XYZ}
            </div>
        )
    })

    

    return (        
        <>
            <Container 
                style = {{padding:"20px",backgroundColor: "#ddd"}} 
                className = {direction}
            >            
            {draw &&
                drawXYZ
            }
            </Container>
        </>
    )
}

export default DrawXYZ
